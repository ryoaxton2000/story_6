from django.test import TestCase, Client
from django.urls import resolve
from . views import home_page
from . models import Status
from . forms import Status_Form

from selenium import webdriver
from django.test import LiveServerTestCase
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
c = Client()

class Status_Test(TestCase):
    def test_ada_link(self):
        response = c.get('')
        self.assertEqual(response.status_code, 200)

    def test_link_tidak_ada(self):
        response = c.get('/hei/')
        self.assertEqual(response.status_code, 404)

    def test_pake_funct(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)
        
    def test_pakai_base_template(self):
        response = c.get('')
        self.assertTemplateUsed(response, 'base.html')

    def test_halo(self):
        response = c.get('')
        content = response.content.decode("utf8")
        self.assertIn("Halo, apa kabar?", content)

    def test_status_model(self):
        Status.objects.create(status="test bro")
        hitungJumlah = Status.objects.all().count()
        self.assertEqual(hitungJumlah, 1)

    def test_input_form(self):
        response = c.post('', data={'status':"test"})
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn("test", content)

    def test_input_overload(self):
        case = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
        form = Status_Form(data={'status':case})
        self.assertFalse(form.is_valid())

    def test_ada_form(self):
        response = c.get('')
        content = response.content.decode("utf8")
        case2 = "<form"
        self.assertIn(case2, content)

class Functional_Test(LiveServerTestCase):
    def setUp(self):
        # options = Options()
        # self.browser = webdriver.Firefox(executable_path="DRIVER/geckodriver")
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser  = webdriver.Chrome(chrome_options=chrome_options)

    
    def tearDown(self):
        self.browser.quit()

    def test_masukan_status_functional(self):
        self.browser.get(self.live_server_url)
        temp_input = self.browser.find_element_by_name('status')
        temp = self.browser.find_element_by_class_name('tombol_simpan')
        temp_input.send_keys("test")
        temp.click()
        self.assertIn("test", self.browser.page_source)
    